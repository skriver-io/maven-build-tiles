# Skriver.io Maven Plugin Configuration Tiles

This project contains customized Maven Plugin Configurations called Tiles which are intended for use by Spring Boot based services or other Maven based projects.
The Tiles enable a Maven project to "declare" its use of a plugin similar to a normal Maven dependency.  
Instead of having to duplicate an often verbose configuration section for a plugin in the pom.xml and risking configuration drift and errors, projects can avoid this by using the [Tiles Maven Plugin](https://github.com/repaint-io/maven-tiles) and the Tiles provided in this project.  

Section overview:

* How to use Tiles in your project
* Skriver.io Plugin Configuration Tiles - an explanation of the provided tiles and how to use them 
* How to add support for a new plugin configuration using tiles
* How to build and release a new version of this project

## How to use Tiles

Add the following snippet to the `pom.xml` file for the given Maven project:  

```xml
  <plugin>
    <groupId>io.repaint.maven</groupId>
    <artifactId>tiles-maven-plugin</artifactId>
    <version>2.15</version>
    <extensions>true</extensions>
    <configuration>
      <filtering>false</filtering>
      <tiles>
        <tile>io.skriver.oss:semver-release-tile:1.0.0</tile>
      </tiles>
    </configuration>
  </plugin>
```

In the `<tiles>` section of the `<configuration>` section, add a `<tile>` line specifying the tile using Maven coordinate - groupId:artifactId:version. 

## Skriver.io Plugin Configuration Tiles

### Semver Release Tile 

The [Semver Release Tile](./semver-release-tile) contains a predefined plugin configuration for supporting GitFlow in Spring Boot services by using the [Git-Flow Maven Plugin](https://aleksandr-m.github.io/gitflow-maven-plugin/index.html).  

The tile supports creating releases following the [Semantic Versioning](https://semver.org/spec/v2.0.0.html)(semver) model.  
The Maven `version` can easily adhere to the semver model since the version format `x.y.z-classifer` maps directly to semver's fields `major.minor.revision`.  

The tile extends the GitFlow plugin by creating either a major, minor or revision release which dictates which of the versions values will be incremented by 1.  
To control which type of release to make, supply the system property `release=<type>` when creating the release. 

To create a major release in a project, run:
```shell script
mvn -B validate gitflow:release -Drelease=major
```

The same goes when creating release branch which is closed at later stage using the two plugin goals `gitflow:release-start` and `gitflow:release-finish`.  
```shell script
mvn -B gitflow:release-start -Drelease=major
mvn -B gitflow:release-finish
```

In addition, the tile allows the use of the GitFlow plugin's other goals for creating feature and hotfix branches - however the applied `version` value, is always based on the branch from which feature or hotfix has been created. 

## How to add support for a new build plugin using tiles

See [Maven Tile Examples](https://github.com/avaje-pom) for inspiration as well as the tiles already in the project.

## How to build and release a new version of the project

This project does not make use of the Release Tile as it is not allowed to use the Release tile sub-module in the parent POM file.  
The same Git-Flow release plugin is used by this project, but the tile's plugin configuration has been duplicated in the parent POM file.  
  
**Remember to update the [CHANGE LOG](./CHANGELOG.md) before releasing a new version.**